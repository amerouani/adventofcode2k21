def fileToDict(file):
    arr = []
    with open(file) as f:
        for line in f:
            arr.append([line.split(' ')[0], int(line.split(' ')[1].strip('\n'))])
    return arr

def part1():
    arr = fileToDict("input.txt")
    depth = 0
    horizontal = 0
    aim = 0

    for i in arr:
        if(i[0] == "forward"):
            horizontal += i[1]
        elif(i[0] == "down"):
            depth += i[1]
        elif(i[0] == "up"):
            depth -= i[1]
    print(horizontal * depth)

def part2():
    arr = fileToDict("input.txt")
    depth = 0
    horizontal = 0
    aim = 0

    for i in arr:
        if(i[0] == "forward"):
            horizontal += i[1]
            depth += aim * i[1]
        elif(i[0] == "down"):
            aim += i[1]
        elif(i[0] == "up"):
            aim -= i[1]
    print(horizontal * depth)
    
