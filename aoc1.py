def fileToArray(file):
    tab = []
    with open(file) as f:
        for line in f:
            tab.append(int(line))
        
    return tab

def part1():
    arr = fileToArray("input.txt")
    lastLine = None
    count = 0
    for i in range(len(arr)):
        if(lastLine == None):
            lastLine = arr[i]
            continue
        else:
            currentLine = arr[i]
            if(lastLine < currentLine):
                count += 1
            lastLine = currentLine
            
    print(count)

def part2():
    arr = fileToArray("input.txt")
    lastSum = None
    count = 0
    for i in range(len(arr)):
        if(lastSum == None):
            lastSum = arr[i] + arr[i + 1] + arr[i + 2]
            continue
        elif((i + 2) >= len(arr)):
            print(count)
            exit()
        else:
            currentSum = arr[i] + arr[i + 1] + arr[i + 2]
            if(currentSum > lastSum):
                count = count + 1
            lastSum = currentSum
