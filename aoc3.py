import collections

def fileToArr(file):
    arr = []
    with open(file) as f:
        for line in f:
            arr.append(line.strip('\n'))
    return arr

def part1():
    arr = fileToArr("input.txt")

    tab = []
    result = []

    epsilon = []
    gamma = []

    for i in range(len(arr[0])):
        for j in range(len(arr)):
            tab.append(arr[j][i])
        result.append(collections.Counter(tab).most_common(2))
        tab = []

    for i in result:
        gamma.append(i[0][0])
        epsilon.append(i[1][0])

    gamma = ''.join(gamma)
    epsilon = ''.join(epsilon)

    print(int(epsilon, 2) * int(gamma, 2))

def part2():
    arr = fileToArr("input.txt")
    print(int(getOxygen(arr, 0)[0], 2) * int(getCo2(arr, 0)[0],2))

def getOxygen(arr, index):
    if(len(arr) > 1):
        bit = getMostCommonBit(arr, index)
        tab = []
        for i in arr:
            if(i[index] == bit):
                tab.append(i)
        return getOxygen(tab, index+1)
    else:
        return arr

def getCo2(arr, index):
    if(len(arr) > 1):
        bit = getLeastCommonBit(arr, index)
        tab = []
        for i in arr:
            if(i[index] == bit):
                tab.append(i)
        return getCo2(tab, index+1)
    else:
       return arr


def getMostCommonBit(arr, index):
    tab = []
    for i in arr:
        tab.append(i[index])

    result = collections.Counter(tab).most_common(2)

    if(result[0][1] == result[1][1]):
        return '1'
    else:
        return result[0][0]

def getLeastCommonBit(arr, index):
    tab = []
    for i in arr:
        tab.append(i[index])

    result = collections.Counter(tab).most_common(2)

    if(result[0][1] == result[1][1]):
        return '0'
    else:
        return result[1][0]

part2()
