import re

def fileToArr(file):
    arr = []
    tab = []
    with open(file) as f:
        for line in f:
            if(line == '\n'):
                continue
            arr.append(line.strip('\n'))
    tab.append(arr[0].split(","))
    for i in range(1, len(arr), 5):
        tab.append([list(filter(None, re.split("[ ]", arr[i + j]))) for j in range(0, 5)])
    
    return tab
    
def part1():
    arr = fileToArr("input.txt")
    print(bingo(arr[0], arr[1:]))

def bingo(draw, boards):
    original = boards
    sum = 0
    finalDraw = ""
    for i in draw:
        markBoards(i, boards)
        verifyBoards(boards)
        if(verifyBoards(boards) != 0):
            sum = sumUnmarkedNumbers(verifyBoards(boards))
            finalDraw = i
            return sum * int(finalDraw)
    return 0
    
def markBoards(draw, boards):
    for i in range(len(boards)):
        for j in range(len(boards[i])):
            for k in range(len(boards[i][j])):
                if(boards[i][j][k] == draw):
                    boards[i][j][k] = "O"

def verifyBoards(boards):
    for i in range(len(boards)):
        for j in range(len(boards[i])):
            if(boards[i][j] == ["O"] * len(boards[i])):
               return boards[i]
    for i in range(len(boards)):
        for j in range(len(boards[i])):
            tab = [row[j] for row in boards[i]]
            if(tab == ["O"] * len(boards[i])):
                return boards[i]
    return 0


def sumUnmarkedNumbers(board):
    sum = 0
    for i in range(len(board)):
        for j in range(len(board[i])):
            if(board[i][j] != "O"):
                sum += int(board[i][j])
    return sum

def bingoPart2(draw, boards):
    original = boards
    sum = 0
    finalDraw = ""
    for i in draw:
        markBoards(i, boards)
        if(verifyBoardsPart2(boards) != 0):
            sum = sumUnmarkedNumbers(verifyBoardsPart2(boards))
            finalDraw = i
            return sum * int(finalDraw)
    return 0

def verifyBoardsPart2(boards):
    index = []
    for i in range(len(boards)):
        for j in range(len(boards[i])):
            if(boards[i][j] == ["O"] * len(boards[i])):
                if(i not in index and len(index) == len(boards) - 1):
                   return boards[i]
                elif(i not in index):
                    index.append(i)
    for i in range(len(boards)):
        for j in range(len(boards[i])):
            tab = [row[j] for row in boards[i]]
            if(tab == ["O"] * len(boards[i])):
                if(i not in index and len(index) == len(boards) - 1):
                   return boards[i]
                elif(i not in index):
                    index.append(i)
    return 0
